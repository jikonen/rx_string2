#include <msp430g2553.h>
#include <inttypes.h>
#include <string.h>
#include "uart.h"

/*
 * main.c
 */

/* Readable pin defines and other constants */
#define RLED 	BIT0
#define GLED	BIT6
#define RX		BIT1
#define TX		BIT2

/* FUNCTION DECLARATIONS */
void board_setup(void);
void delay_cycles(volatile uint32_t i);


/* GLOBAL VARIABLES */
char rx_string[8];


int main(void) {
    WDTCTL = WDTPW | WDTHOLD;			// Stop watchdog timer
    BCSCTL1 = CALBC1_8MHZ; 				//Set DCO to 8Mhz
    DCOCTL = CALDCO_8MHZ; 				//Set DCO to 8Mhz

    board_setup();
    uart_init();
    __enable_interrupt();




    while(1) {

    	if(rx_flag == 1) {
    		uart_putc(uart_getc());			// getc fixes one line lag on reading commands somehow
    										// getting empty line on console means faulty command
    		uart_gets(rx_string, 8);		// se on ominaisuus ei bugi
/*
    		if(rx_string[1] == 'a') {
    			uart_puts(rx_string);
    			P1OUT ^= RLED;
    		} */
    		if(strcmp(rx_string, "rled") == 0) {
    			uart_puts(rx_string);
    			P1OUT ^= RLED;
    		}
    	}


    }

	return 0;
}


/* Pin and other setup at start */
void board_setup(void) {

	P1OUT = 0;
	P1DIR |= RLED + GLED;

}

/* Delay i cycles */
void delay_cycles(volatile uint32_t i) {
	do i--;
	while(i != 0);
}


